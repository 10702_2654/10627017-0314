package sample;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;


public class Controller {
    public int a = 0, b = 0;
    public String z="";
    public Label lcd;
    public Button btn1;
    public Button btn2;
    public Button btn3;
    public Button btn4;
    public Button btn5;
    public Button btn6;
    public Button btn7;
    public Button btn8;
    public Button btn9;
    public Button btn0;
    public Button btnplus;
    public Button btnminus;
    public Button btnX;
    public Button btnl;
    public Button btndot;
    public Button btnequal;
    public Button btnC;
    public Button btnAC;

    public void onClick(ActionEvent actionEvent) {
        Button button = (Button) actionEvent.getSource();
        switch (button.getText()) {
            case "+":
                z="+";
                a+=Integer.parseInt(lcd.getText());
                lcd.setText("");
                break;
            case "-":
                z="-";
                a+=Integer.parseInt(lcd.getText());
                lcd.setText("");
                break;
            case "*":
                z="*";
                a+=Integer.parseInt(lcd.getText());
                lcd.setText("");
                break;
            case "/":
                z="/";
                a+=Integer.parseInt(lcd.getText());
                lcd.setText("");
                break;
            case "=":
                b=Integer.parseInt(lcd.getText());
                switch(z){
                    case "+":
                        lcd.setText(String.valueOf(a+b));
                        break;
                    case "-":
                        lcd.setText(String.valueOf(a-b));
                        break;
                    case "*":
                        lcd.setText(String.valueOf(a*b));
                        break;
                    case "/":
                        lcd.setText(String.valueOf(a/b));
                        break;
                }
                //lcd.setText(String.valueOf(a));
                break;
            case "AC":
                lcd.setText("");
                a=0;
                b=0;
                break;
            case "C":
                lcd.setText("");
                break;
        }
    }

    public void onNum(ActionEvent actionEvent) {
        Button button=(Button) actionEvent.getSource();
        String number = lcd.getText();
        lcd.setText(number.concat(button.getText())); 
    }
}